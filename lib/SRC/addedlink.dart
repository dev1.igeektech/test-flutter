import 'dart:convert';

import 'package:demo/SRC/GLOBAL/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddedlinkPage extends StatefulWidget {
  final Map dic;
  AddedlinkPage({this.dic});
  @override
  _AddedlinkPageState createState() => _AddedlinkPageState(dic: dic);
}

class _AddedlinkPageState extends State<AddedlinkPage> {
  Map dic;
  _AddedlinkPageState({this.dic});
  final _searchQuery = TextEditingController();
//   TextEditingController _email = TextEditingController();
// List<TextEditingController> emailtextcollection = [];
  List datalist = [];
  bool once = true;
  @override
  void initState() {
    if (prefsObject.containsKey('COLLECTION')) {
      datalist = json.decode(prefsObject.getString('COLLECTION')) ?? [];
    } else {
      prefsObject.setString('COLLECTION', json.encode([]));
    }
    super.initState();
    // _searchQuery.text = dic['link'];
  }

  @override
  void dispose() {
    _searchQuery.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey,
              size: 25,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          'Add link',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        centerTitle: false,
        elevation: 1.2,
      ),
      body: Stack(
        children: <Widget>[
          _body(),
        ],
      ),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
              child: ListView.separated(
                  itemCount: datalist.isNotEmpty ? datalist.length : 0,
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(
                        indent: 80,
                      ),
                  itemBuilder: (context, int index) {
                    return ListTile(
                      dense: true,
                      contentPadding:
                          EdgeInsets.only(bottom: 0, left: 15, right: 10),
                      leading: CircleAvatar(
                        backgroundColor: Colors.grey[200],
                        radius: 25,
                        child: Image.asset(
                          'assets/images/${datalist[index]['image']}',
                          fit: BoxFit.cover,
                        ),
                      ),
                      title: Text(
                        'Link Name',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(datalist[index]['link']),
                      // trailing: IconButton(icon: Icon(Icons.delete_outline,color: Colors.grey,size: 28,), onPressed: (){

                      // }),
                    );
                  })),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 10),
            child: _topsearch(),
          )
        ],
      ),
    );
  }

  _topsearch() {
    return TextField(
      cursorColor: Colors.black,
      textCapitalization: TextCapitalization.sentences,
      controller: _searchQuery,
      style: TextStyle(color: Colors.black, fontSize: 14),
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.only(
          left: 20,
          right: 5,
        ),
        focusColor: Colors.black,
        hintStyle: TextStyle(
          fontFamily: 'Iceland_Regular',
          fontSize: 14.0,
        ),
        // prefixIcon: Icon(
        //   Icons.search,
        //   color: Colors.black,
        // ),
        suffixIcon: Padding(
          padding: EdgeInsets.only(right: 2),
          child: IconButton(
              icon: Icon(
                Icons.send,
                color: Colors.grey,
              ),
              onPressed: () {
                if (_searchQuery.text != '') {
                  if (prefsObject.containsKey('COLLECTION')) {
                    List datacol =
                        json.decode(prefsObject.getString('COLLECTION')) ?? [];

                    print({'image': dic['image'], 'link': _searchQuery.text});
                    datacol.add(
                        {'image': dic['image'], 'link': _searchQuery.text});

                    prefsObject.setString('COLLECTION', json.encode(datacol));
                    _searchQuery.text = '';
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    FocusScope.of(context).requestFocus(FocusNode());
                    datalist = datacol;
                    setState(() {});
                  } else {
                    prefsObject.setString('COLLECTION', json.encode([]));
                    _searchQuery.text = '';
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    FocusScope.of(context).requestFocus(FocusNode());
                  }
                } else {
                  _searchQuery.text = '';
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                  FocusScope.of(context).requestFocus(FocusNode());
                }
              }),
        ),
        hintText: 'Past link here',
        // labelText: 'Search sites',

        labelStyle: TextStyle(color: Colors.black),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
          borderSide: BorderSide(color: Colors.grey),
        ),
      ),
      onTap: () {},
    );
  }
}
