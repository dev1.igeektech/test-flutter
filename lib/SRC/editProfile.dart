import 'dart:convert';
import 'dart:io';

import 'package:demo/SRC/TABBAR/default_appbar_demo.dart';
import 'package:demo/SRC/editlink.dart';
import 'package:demo/SRC/socialCollection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'GLOBAL/constant.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController _memo = TextEditingController();
  TextEditingController _name = TextEditingController();

  File image;
  File coverphoto;
  bool isLoading = false;
  // final LoginBloc _loginBloc = LoginBloc();
  List datalist = [];
  @override
  void initState() {
    _name.text = "@My_Profile";
    super.initState();
    if (prefsObject.containsKey('COLLECTION')) {
      datalist = json.decode(prefsObject.getString('COLLECTION')) ?? [];
    } else {
      prefsObject.setString('COLLECTION', json.encode([]));
    }

    if (prefsObject.containsKey('CONTENTINFO')) {
      _memo.text = prefsObject.getString('CONTENTINFO') ?? '';
    }
  }

  @override
  void dispose() {
    super.dispose();
    prefsObject.setString('CONTENTINFO', _memo.text);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Stack(
          children: <Widget>[
            Scaffold(
                body: Stack(
              children: <Widget>[
                ModalProgressHUD(
                  inAsyncCall: isLoading,
                  child: homeForm(),
                  progressIndicator: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(myBlackColor),
                  ),
                ),
                Positioned(
                    top: 30,
                    child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          if (_memo.text != null) {
                            prefsObject.setString('CONTENTINFO', _memo.text);
                          }

                          Navigator.of(context).pushAndRemoveUntil(
                              NoAnimationMaterialPageRoute(
                                  builder: (context) => DefaultAppBarDemo()),
                              (Route<dynamic> route) => false);
                        })),
                Positioned(
                  right: 15,
                  top: 40,
                  child: GestureDetector(
                      onTap: () {
                        containerForSheet<String>(
                          context: context,
                          child: CupertinoActionSheet(
                            actions: <Widget>[
                              CupertinoActionSheetAction(
                                child: Text(
                                  'Camera',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                                onPressed: () async {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop("Discard");
                                  image = await ImagePicker.pickImage(
                                      source: ImageSource.camera);
                                  setState(() {});
                                  if (image != null) {
                                    prefsObject.setString(
                                        'IMAGE', image.toString());
                                  }
                                },
                              ),
                              CupertinoActionSheetAction(
                                child: Text(
                                  'Gallery',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                                onPressed: () async {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop("Discard");
                                  image = await ImagePicker.pickImage(
                                      source: ImageSource.gallery);
                                  setState(() {});
                                  if (image != null) {
                                    prefsObject.setString(
                                        'IMAGE', image.toString());
                                  }
                                },
                              ),
                            ],
                            cancelButton: CupertinoActionSheetAction(
                              child: Text(
                                'Cancel',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              isDefaultAction: true,
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop("Discard");
                              },
                            ),
                          ),
                        );
                      },
                      child: Image.asset(
                        'assets/images/camera.png',
                        height: 28,
                        width: 28,
                        color: Colors.white,
                      )),
                ),
              ],
            )),
          ],
        ));
  }

  Widget homeForm() {
    return commonBottomWidgetAreaForEdit(
      takephoto: () {
        containerForSheet<String>(
          context: context,
          child: CupertinoActionSheet(
            actions: <Widget>[
              CupertinoActionSheetAction(
                child: Text(
                  'Camera',
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                onPressed: () async {
                  Navigator.of(context, rootNavigator: true).pop("Discard");
                  coverphoto =
                      await ImagePicker.pickImage(source: ImageSource.camera);
                  setState(() {});
                  if (coverphoto != null) {
                    prefsObject.setString('COVERPHOTO', coverphoto.path);
                  }
                },
              ),
              CupertinoActionSheetAction(
                child: Text(
                  'Gallery',
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                onPressed: () async {
                  Navigator.of(context, rootNavigator: true).pop("Discard");
                  coverphoto =
                      await ImagePicker.pickImage(source: ImageSource.gallery);
                  setState(() {});
                  if (coverphoto != null) {
                    prefsObject.setString('COVERPHOTO', coverphoto.path);
                  }
                },
              ),
            ],
            cancelButton: CupertinoActionSheetAction(
              child: Text(
                'Cancel',
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              isDefaultAction: true,
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop("Discard");
              },
            ),
          ),
        );
      },
      context: context,
      appTitleWidget: Container(
          // color: Colors.grey[200],
          ),
      children: [
        SizedBox(
          height: 6,
        ),
        _topline(),
        SizedBox(
          height: 25,
        ),
        _profileDetails(),
        SizedBox(
          height: 10,
        ),
        _contentINFO(),
        SizedBox(
          height: 15,
        ),
        socialAvtarlist(),
      ],
    );
  }

  _topline() {
    return Container(
      width: MediaQuery.of(context).size.width / 3.6,
      height: 2,
      decoration: BoxDecoration(
          color: Colors.grey, borderRadius: BorderRadius.circular(2)),
    );
  }

  void containerForSheet<T>({BuildContext context, Widget child}) {
    showCupertinoModalPopup<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {});
  }

  _profileDetails() {
    var getimage;

    if (prefsObject.containsKey('IMAGE')) {
      getimage = prefsObject
          .getString('IMAGE')
          .replaceAll('File: ', '')
          .replaceAll("'", "");
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Stack(
          alignment: Alignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                containerForSheet<String>(
                  context: context,
                  child: CupertinoActionSheet(
                    actions: <Widget>[
                      CupertinoActionSheetAction(
                        child: Text(
                          'Camera',
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                        onPressed: () async {
                          Navigator.of(context, rootNavigator: true)
                              .pop("Discard");
                          await ImagePicker.pickImage(
                                  source: ImageSource.camera)
                              .then((onValue) {
                            image = onValue;
                            setState(() {});
                            if (image != null) {
                              prefsObject.setString('IMAGE', image.toString());
                            }
                          });
                        },
                      ),
                      CupertinoActionSheetAction(
                          child: Text(
                            'Gallery',
                            style: TextStyle(color: Colors.black, fontSize: 15),
                          ),
                          onPressed: () async {
                            Navigator.of(context, rootNavigator: true)
                                .pop("Discard");
                            image = await ImagePicker.pickImage(
                                source: ImageSource.gallery);
                            setState(() {});
                            if (image != null) {
                              prefsObject.setString('IMAGE', image.toString());
                            }
                          }),
                    ],
                    cancelButton: CupertinoActionSheetAction(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      isDefaultAction: true,
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop("Discard");
                      },
                    ),
                  ),
                );
              },
              child: CircleAvatar(
                radius: 30.0,
                backgroundImage: getimage != null
                    ? FileImage(File(getimage))
                    :
                    // image != null ? FileImage(image):
                    NetworkImage(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSlisndS_tsbb3Z_WafFH_2O8gU9i0L46qWvw&usqp=CAU"),
                backgroundColor: Colors.transparent,
              ),
            ),
            GestureDetector(
                onTap: () {
                  containerForSheet<String>(
                    context: context,
                    child: CupertinoActionSheet(
                      actions: <Widget>[
                        CupertinoActionSheetAction(
                          child: Text(
                            'Camera',
                            style: TextStyle(color: Colors.black, fontSize: 15),
                          ),
                          onPressed: () async {
                            Navigator.of(context, rootNavigator: true)
                                .pop("Discard");
                            image = await ImagePicker.pickImage(
                                source: ImageSource.camera);
                            setState(() {});
                            if (image != null) {
                              prefsObject.setString('IMAGE', image.toString());
                            }
                          },
                        ),
                        CupertinoActionSheetAction(
                          child: Text(
                            'Gallery',
                            style: TextStyle(color: Colors.black, fontSize: 15),
                          ),
                          onPressed: () async {
                            Navigator.of(context, rootNavigator: true)
                                .pop("Discard");
                            image = await ImagePicker.pickImage(
                                source: ImageSource.gallery);
                            setState(() {});
                            if (image != null) {
                              prefsObject.setString('IMAGE', image.toString());
                            }
                          },
                        ),
                      ],
                      cancelButton: CupertinoActionSheetAction(
                        child: Text(
                          'Cancel',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        isDefaultAction: true,
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true)
                              .pop("Discard");
                        },
                      ),
                    ),
                  );
                },
                child: Image.asset(
                  'assets/images/camera.png',
                  height: 30,
                  width: 30,
                  color: Colors.white,
                )),
          ],
        ),
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                // width: MediaQuery.of(context).size.width -30,

                child: SizedBox(
                  height: 30,
                  child: TextField(
                    cursorColor: Colors.black,
                    // focusNode: _memo,
                    keyboardType: TextInputType.text,
                    onChanged: (text) {},
                    controller: _name,

                    textInputAction: TextInputAction.done,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16),
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      // suffixIcon: Image.asset('assets/images/locationcircle.png'),
                      contentPadding: const EdgeInsets.all(0),
                      // labelText: 'Notes',
                      labelStyle: TextStyle(
                          color: Color(0xff42436A),
                          fontFamily: 'Iceland_Regular'),
                      focusColor: Colors.black,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.transparent, width: 1.1),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.transparent, width: 1.1),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6),
                          borderSide: BorderSide(
                              color: Colors.transparent, width: 1.1)),
                    ),
                    onTap: () {},
                  ),
                ),
              ),
              // Text('@My_Profile',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 16),textAlign: TextAlign.left,),

              Text(
                'Brisbane, Australia',
                style: TextStyle(color: Colors.black, fontSize: 12),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
      ],
    );
  }

  void choiceAction(String choice) {
    if (choice == Constants.Settings) {
      print('Settings');
    } else if (choice == Constants.Edit) {
      print('Edit');
    } else if (choice == Constants.Interests) {
      print('SignOut');
    } else if (choice == Constants.Logout) {
      print('LogOut');
    }
  }

  _contentINFO() {
    return Container(
      // width: MediaQuery.of(context).size.width -30,

      child: TextField(
        cursorColor: Colors.black,
        // focusNode: _memo,
        keyboardType: TextInputType.text,
        onChanged: (text) {},
        controller: _memo,
        maxLines: 3,
        textInputAction: TextInputAction.done,
        inputFormatters: <TextInputFormatter>[
          LengthLimitingTextInputFormatter(1000),
        ],
        onSubmitted: (value) {},
        decoration: InputDecoration(
          hintText: 'Add bio',
          // suffixIcon: Image.asset('assets/images/locationcircle.png'),
          contentPadding: const EdgeInsets.all(0),
          // labelText: 'Notes',
          labelStyle: TextStyle(
              color: Color(0xff42436A), fontFamily: 'Iceland_Regular'),
          focusColor: Colors.black,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent, width: 1.1),
            borderRadius: BorderRadius.circular(6),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent, width: 1.1),
            borderRadius: BorderRadius.circular(6),
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(6),
              borderSide: BorderSide(color: Colors.transparent, width: 1.1)),
        ),
        onTap: () {},
      ),
    );

    // Container(
    //       child: Text("This is a great place to tell perople a little bit about yourself. Don't fonget to add your social links via settings so they know where they can follow you."),
    // );
  }

  List data = [
    {
      'image': 'contact.png',
    },
    {
      'image': 'fb.png',
    },
    {
      'image': 'insta.png',
    },
    {
      'image': 'linkedin.png',
    },
    {
      'image': 'm.png',
    },
    {
      'image': 'mail.png',
    },
    {
      'image': 'social.png',
    },
    {
      'image': 'tiktok.png',
    },
    {
      'image': 'rotate.png',
    },
  ];
  socialAvtarlist() {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            if (_memo.text != null) {
              prefsObject.setString('CONTENTINFO', _memo.text);
            }
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SocialCollectionPage()));
          },
          child: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                shape: BoxShape.circle, border: Border.all(color: Colors.grey)),
            child: Icon(
              Icons.add,
              color: Colors.grey,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Container(
            height: 40,
            margin: EdgeInsets.all(0),
            child: ListView.builder(
                padding: EdgeInsets.all(0),
                itemCount: datalist.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, int index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: GestureDetector(
                      onTap: () {
                        if (_memo.text != null) {
                          prefsObject.setString('CONTENTINFO', _memo.text);
                        }
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LinkEditPage(
                                    dic: datalist[index], selected: index)));
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(shape: BoxShape.circle),
                        child: Image.asset(
                            'assets/images/${datalist[index]['image']}'),
                      ),
                    ),
                  );

                  // CircleAvatar(
                  //     radius: 45,
                  //     child: Image.asset('assets/images/${data[index]['image']}'),
                  //     backgroundColor: Colors.transparent,
                  //   );
                }),
          ),
        ),
      ],
    );
  }
}
