import 'package:demo/SRC/Language/translations_delegates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'GLOBAL/constant.dart';
import 'TABBAR/default_appbar_demo.dart';

class MyApp extends StatelessWidget {
  const MyApp({this.prefs});
  final SharedPreferences prefs;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        textTheme: GoogleFonts.quicksandTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      //  initialRoute: "/",
      routes: {
        // "/": (BuildContext context) => DefaultAppBarDemo(),
        // "/custom": (BuildContext context) => CustomAppBarDemo(),
      },
      localizationsDelegates: [
        const TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      title: 'The Race Card',
      home: _handleCurrentScreen(prefs),
    );
  }

  Widget _handleCurrentScreen(SharedPreferences pref) {
    if (pref.getKeys().isEmpty) {
      prefsObject = pref;
      return DefaultAppBarDemo();
    } else {
      prefsObject = pref;
      return DefaultAppBarDemo();
    }
  }
}
