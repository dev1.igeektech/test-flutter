import 'dart:convert';

import 'package:demo/SRC/GLOBAL/constant.dart';
import 'package:demo/SRC/addedlink.dart';
import 'package:demo/SRC/editProfile.dart';
import 'package:demo/SRC/editlink.dart';
import 'package:flutter/material.dart';

class SocialCollectionPage extends StatefulWidget {
  @override
  _SocialCollectionPageState createState() => _SocialCollectionPageState();
}

class _SocialCollectionPageState extends State<SocialCollectionPage> {
  List datalist = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 25,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.grey,
                          size: 25,
                        ),
                        onPressed: () {
                          // Navigator.pop(context);
                          Navigator.of(context).pushAndRemoveUntil(
                              NoAnimationMaterialPageRoute(
                                  builder: (context) => EditProfilePage()),
                              (Route<dynamic> route) => false);
                        }),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25, right: 25),
                    child: Text(
                      'Help people to find you by sharing content from other profiles directly to Miit and use your Miit user name to publish your profile anywhere.',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    )),
                SizedBox(
                  height: 20,
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25, right: 25),
                    child: Text(
                      "We don't share anything without your permission.",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    )),
                SizedBox(
                  height: 10,
                ),
                _gridviewshow(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _gridviewshow() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 25, right: 25),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4, crossAxisSpacing: 20.0, mainAxisSpacing: 20),
          itemBuilder: (_, index) {
            return GestureDetector(
              onTap: () {
                print(data[index]);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddedlinkPage(dic: data[index])));
              },
              child: CircleAvatar(
                backgroundColor: Colors.grey[200],
                radius: 30,
                child: Image.asset(
                  'assets/images/${data[index]['image']}',
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
          itemCount: data.length,
        ),
      ),
    );
  }
}
