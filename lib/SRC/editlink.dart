import 'dart:convert';

import 'package:demo/SRC/GLOBAL/constant.dart';
import 'package:demo/SRC/addedlink.dart';
import 'package:demo/SRC/editProfile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LinkEditPage extends StatefulWidget {
  final Map dic;
  final int selected;
  LinkEditPage({this.dic, this.selected});
  @override
  _LinkEditPageState createState() =>
      _LinkEditPageState(dic: dic, selected: selected);
}

class _LinkEditPageState extends State<LinkEditPage> {
  Map dic;
  int selected;
  _LinkEditPageState({this.dic, this.selected});
  final _searchQuery = TextEditingController();

  List<TextEditingController> emailtextcollection = [];
  List datalist = [];
  bool once = true;
  @override
  void initState() {
    _searchQuery.text = dic['link'];

    super.initState();
    emailtextcollection = [];
    if (prefsObject.containsKey('COLLECTION')) {
      datalist = json.decode(prefsObject.getString('COLLECTION')) ?? [];
    } else {
      prefsObject.setString('COLLECTION', json.encode([]));
    }
  }

  @override
  void dispose() {
    _searchQuery.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey,
              size: 25,
            ),
            onPressed: () {
              // Navigator.pop(context);
              Navigator.of(context).pushAndRemoveUntil(
                  NoAnimationMaterialPageRoute(
                      builder: (context) => EditProfilePage()),
                  (Route<dynamic> route) => false);
            }),
        title: Text(
          'Edit link',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        centerTitle: false,
        elevation: 1.2,
      ),
      body: Stack(
        children: <Widget>[
          // body(),
          Container(
            padding: EdgeInsets.only(top: 15, bottom: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: ListView.separated(
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                              indent: 80,
                            ),
                        itemCount: datalist.length,
                        itemBuilder: (context, int index) {
                          if (once) {
                            for (int i = 0; i < datalist.length; i++) {
                              emailtextcollection.add(TextEditingController(
                                  text: datalist[i]['link']));
                            }

                            once = false;
                          }
                          return GestureDetector(
                            onTap: () {
                              selected = index;
                              dic = datalist[index];
                              _searchQuery.text = datalist[index]['link'];
                            },
                            child: ListTile(
                              dense: true,
                              contentPadding: EdgeInsets.only(
                                  bottom: 0, left: 15, right: 10),
                              leading: CircleAvatar(
                                backgroundColor: Colors.grey[200],
                                radius: 25,
                                child: Image.asset(
                                  'assets/images/${datalist[index]['image']}',
                                  fit: BoxFit.cover,
                                ),
                              ),
                              title: Text(
                                'Link Name (e.g Email)',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(datalist[index]['link']),
                              trailing: IconButton(
                                  icon: Icon(
                                    Icons.delete_outline,
                                    color: Colors.grey,
                                    size: 28,
                                  ),
                                  onPressed: () {
                                    List datacol = json.decode(prefsObject
                                            .getString('COLLECTION')) ??
                                        [];
                                    datacol.removeAt(index);
                                    datalist = datacol;
                                    setState(() {});
                                    prefsObject.setString(
                                        'COLLECTION', jsonEncode(datalist));
                                  }),
                            ),
                          );
                        })),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15, top: 10),
                  child: topsearch(context),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
//  body()
//  {
//    return ;
//  }

  Widget topsearch(BuildContext context) {
    return TextField(
      cursorColor: Colors.black,
      textCapitalization: TextCapitalization.sentences,
      controller: _searchQuery,
      style: TextStyle(color: Colors.black, fontSize: 14),
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.only(
          left: 20,
          right: 5,
        ),
        focusColor: Colors.black,
        hintStyle: TextStyle(
          fontFamily: 'Iceland_Regular',
          fontSize: 14.0,
        ),

        suffixIcon: Padding(
          padding: EdgeInsets.only(right: 2),
          child: IconButton(
              icon: Icon(
                Icons.send,
                color: Colors.grey,
              ),
              onPressed: () {
                if (_searchQuery.text != '') {
                  List datacol =
                      json.decode(prefsObject.getString('COLLECTION')) ?? [];
                  datacol.removeAt(selected);
                  Map doc = {'image': dic['image'], 'link': _searchQuery.text};
                  datacol.insert(selected, doc);
                  datalist = datacol;
                  prefsObject.setString('COLLECTION', jsonEncode(datalist));
                }
                _searchQuery.text = '';
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                FocusScope.of(context).requestFocus(FocusNode());
                setState(() {});
              }),
        ),
        hintText: 'Past link here',
        // labelText: 'Search sites',

        labelStyle: TextStyle(color: Colors.black),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
          borderSide: BorderSide(color: Colors.grey),
        ),
      ),
      onTap: () {},
    );
  }
}
