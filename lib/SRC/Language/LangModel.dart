/*
This class extends Scoped Model and it wraps the whole App to control the language
 */

import 'package:demo/SRC/GLOBAL/constant.dart';
import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';

class LangModel extends Model {
  static const Locale arLocale = Locale('hi');
  static const Locale enLocale = Locale('en');
  String localeStr;

  appModel() {
    handleLocale();
  }

  Locale _appLocale;

  Locale get appLocal {
    handleLocale();
    notifyListeners();
    return _appLocale;
  }

  List<Locale> get supportedLocales => [
        enLocale,
        arLocale,
      ];

  handleLocale(){

    if (prefsObject.getString('locale') == 'hi') {
      ConstantValue.appLanguage = 'hi';
      _appLocale = arLocale;
    } else {
      ConstantValue.appLanguage = 'en';
      _appLocale = enLocale;
    }
  }

  Future<String> localeString() async {

    return prefsObject.getString('locale');
  }

  String getLocalStr() {
    if (appLocal == arLocale)
      return 'hi';
    else
      return 'en';
  }

  void changeLanguage() {
    if (_appLocale == arLocale) {
      _appLocale = enLocale;
      saveLocale('en');
      ConstantValue.appLanguage = 'en';
    } else {
      _appLocale = arLocale;
      saveLocale('ar');
      ConstantValue.appLanguage = 'hi';
    }
    notifyListeners();
  }

  void saveLocale(String loc) async {
    // SharedPreferences pref = await SharedPreferences.getInstance();
    prefsObject.setString('locale', loc);
    ConstantValue.appLanguage = loc;
  }
}
