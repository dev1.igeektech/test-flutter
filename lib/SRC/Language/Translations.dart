import 'package:flutter/widgets.dart';

class Translations {
  Translations(this.locale);

  final Locale locale;

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      "myProfile":'My Profile',
      'chat':'Chat',
      'activity':'Activity',
      'location':'Location',
      'home':'Home',
    },

    //______________________________________________-ar
    'hi': {
      "myProfile":'मेरी प्रोफाइल',
      'chat':'चैट',
      'activity':'गतिविधि',
      'location':'स्थान',
      'home':'घर',
    },
  };

  String get myProfile {
    return _localizedValues[locale.languageCode]['myProfile'];
  }
  String get chat {
    return _localizedValues[locale.languageCode]['chat'];
  }

  String get activity {
    return _localizedValues[locale.languageCode]['activity'];
  }

  String get location {
    return _localizedValues[locale.languageCode]['location'];
  }

  String get home {
    return _localizedValues[locale.languageCode]['home'];
  }


}
