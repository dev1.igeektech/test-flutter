import 'dart:convert';
import 'dart:io';

import 'package:demo/SRC/Language/LangModel.dart';
import 'package:demo/SRC/editProfile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:scoped_model/scoped_model.dart';

import 'GLOBAL/constant.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  File coverphoto;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;
  // final LoginBloc _loginBloc = LoginBloc();
  List datalist = [];
  String bioset;
  @override
  void initState() {
    super.initState();
    if (prefsObject.containsKey('COLLECTION')) {
      datalist = json.decode(prefsObject.getString('COLLECTION')) ?? [];
    } else {
      prefsObject.setString('COLLECTION', json.encode([]));
    }

    if (prefsObject.containsKey('CONTENTINFO')) {
      bioset = prefsObject.getString('CONTENTINFO') ?? '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Stack(
          children: <Widget>[
            Scaffold(
              body: ModalProgressHUD(
                inAsyncCall: isLoading,
                child: homeForm(),
                progressIndicator: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(myBlackColor),
                ),
              ),
            ),
          ],
        ));
  }

  void containerForSheet<T>({BuildContext context, Widget child}) {
    showCupertinoModalPopup<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {});
  }

  Widget homeForm() {
    return commonBottomWidgetArea(
      context: context,
      appTitleWidget: Container(
          // color: Colors.grey[200],
          ),
      children: [
        SizedBox(
          height: 6,
        ),
        _topline(),
        SizedBox(
          height: 25,
        ),
        profileDetails(),
        SizedBox(
          height: 10,
        ),
        contentINFO(),
        SizedBox(
          height: 15,
        ),
        datalist.isNotEmpty ? socialAvtarlist() : SizedBox(),
      ],
    );
  }

  _topline() {
    return Container(
      width: MediaQuery.of(context).size.width / 3.6,
      height: 2,
      decoration: BoxDecoration(
          color: Colors.grey, borderRadius: BorderRadius.circular(2)),
    );
  }

  profileDetails() {
    var image;

    if (prefsObject.containsKey('IMAGE')) {
      image = prefsObject
          .getString('IMAGE')
          .replaceAll('File: ', '')
          .replaceAll("'", "");
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        CircleAvatar(
          radius: 30.0,
          backgroundImage: image != null
              ? FileImage(File(image))
              : NetworkImage(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSlisndS_tsbb3Z_WafFH_2O8gU9i0L46qWvw&usqp=CAU"),
          backgroundColor: Colors.transparent,
        ),
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '@My_Profile',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 16),
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: 6,
              ),
              Text(
                'Brisbane, Australia',
                style: TextStyle(color: Colors.black, fontSize: 12),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
        SizedBox(
          width: 15,
        ),
        ScopedModelDescendant<LangModel>(
          builder: (context, child, model) => InkWell(
            enableFeedback: false,
            onTap: () => model.changeLanguage(),
            child: IconButton(
                icon: Icon(Icons.inbox, color: Colors.grey),
                onPressed: () {
                  model.changeLanguage();
                }),
          ),
        ),
        PopupMenuButton<String>(
          onSelected: choiceAction,
          itemBuilder: (BuildContext context) {
            return Constants.choices.map((String choice) {
              return PopupMenuItem<String>(
                height: 35.0,
                value: choice,
                child: Padding(
                  padding: const EdgeInsets.only(top: 2, bottom: 2),
                  child: Text(choice),
                ),
              );
            }).toList();
          },
        )
      ],
    );
  }

  void choiceAction(String choice) {
    if (choice == Constants.Settings) {
      print('Settings');
    } else if (choice == Constants.Edit) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => EditProfilePage()));
    } else if (choice == Constants.Interests) {
      print('SignOut');
    } else if (choice == Constants.Logout) {
      print('LogOut');
    }
  }

  contentINFO() {
    if (prefsObject.containsKey('CONTENTINFO')) {
      bioset = prefsObject.getString('CONTENTINFO') ?? '';
    }
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Text(
        bioset != null ? bioset : 'Fill up your bio ...',
        textAlign: TextAlign.left,
      ),
    );
  }

  List data = [
    {
      'image': 'contact.png',
    },
    {
      'image': 'fb.png',
    },
    {
      'image': 'insta.png',
    },
    {
      'image': 'linkedin.png',
    },
    {
      'image': 'm.png',
    },
    {
      'image': 'mail.png',
    },
    {
      'image': 'social.png',
    },
    {
      'image': 'tiktok.png',
    },
    {
      'image': 'rotate.png',
    },
  ];
  socialAvtarlist() {
    return Container(
      height: 40,
      margin: EdgeInsets.all(0),
      child: ListView.builder(
          padding: EdgeInsets.all(0),
          itemCount: datalist.isNotEmpty ? datalist.length : 0,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, int index) {
            return Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(shape: BoxShape.circle),
                child: Image.asset('assets/images/${datalist[index]['image']}'),
              ),
            );
          }),
    );
  }
}
