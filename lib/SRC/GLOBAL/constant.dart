import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences prefsObject;

const myPrimaryColor = Color(0xFFFFFFFF);
const mySecondPrimaryColor = Color(0xFF808080);
const myLightGreyColor = Color(0xFFF2F2F2);
const myGreyColor = Color(0xFFC8C8C8);
const myBlackColor = Color(0xFF000000);
const myErrorColor = Color(0xFFFF0000);

//Comon bottom Widget Area
Widget commonBottomWidgetArea(
    {BuildContext context,
    List<Widget> children,
    Widget appTitleWidget,
    bool backIconVisible = false,
    Function onBackButtonPressed,
    Function takephoto,
    var coverimage
    }) {

        var getimage;
  
    if(prefsObject.containsKey('COVERPHOTO'))
    {
      getimage = prefsObject.getString('COVERPHOTO').replaceAll('File: ', '').replaceAll("'", "");
    }

  return Stack(
    children: [
      GestureDetector(
        onTap: takephoto,
              child:getimage != null ? Image.file(File(getimage),
              height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              ) : 
              
              Image.asset(
                "assets/images/bg.png",
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,

              ) ,
      ),
      Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Center(child: appTitleWidget),
            ),
            Container(
              decoration: BoxDecoration(
                color: myPrimaryColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(35),
                  topRight: Radius.circular(35),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
                child: Column(
                  children: children,
                ),
              ),
            ),
            // SizedBox(height: 65,)
          ],
        ),
      ),
      Visibility(
        visible: backIconVisible,
        child: Positioned(
          top: 50,
          left: 15,
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: myPrimaryColor,
            ),
            onPressed: onBackButtonPressed,
          ),
        ),
      ),
    ],
  );
}


//Comon bottom Widget Area
Widget commonBottomWidgetAreaForEdit(
    {BuildContext context,
    List<Widget> children,
    Widget appTitleWidget,
    bool backIconVisible = false,
    Function onBackButtonPressed,
    Function takephoto,
    var coverimage
    }) {

              var getimage;
  
    if(prefsObject.containsKey('COVERPHOTO'))
    {
      getimage = prefsObject.getString('COVERPHOTO').replaceAll('File: ', '').replaceAll("'", "");
    }
  return Stack(
    children: [
      GestureDetector(
        onTap: takephoto,
        child:  getimage != null ? Image.file(File(getimage),
              height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              ) :
      Image.asset(
              "assets/images/bg.png",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,

            ),
      ),
    //  SingleChildScrollView
      Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
      Expanded(
        child: Center(child: appTitleWidget),
      ),
      Container(
        decoration: BoxDecoration(
          color: myPrimaryColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(35),
            topRight: Radius.circular(35),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
          child: Column(
            children: children,
          ),
        ),
      ),
      // SizedBox(height: 65,)
            ],
          ),
        ),
      Visibility(
        visible: backIconVisible,
        child: Positioned(
          top: 50,
          left: 15,
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: myPrimaryColor,
            ),
            onPressed: onBackButtonPressed,
          ),
        ),
      ),
    ],
  );
}

class Constants{
  static const String Edit = 'Edit';
  static const String Settings = 'Settings';
  static const String Interests = 'Interests';
  static const String Logout = 'Logout';

  static const List<String> choices = <String>[
    
    Settings,
    Edit,
    Interests,
    Logout
  ];
}
class NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  NoAnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
            builder: builder,
            maintainState: maintainState,
            settings: settings,
            fullscreenDialog: fullscreenDialog);
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}

List data = [
  {
    'image':'contact.png',
    'link':'http://contact'
  },
  {
    'image':'fb.png',
    'link':'http://fb'
  },
  {
    'image':'insta.png',
    'link':'http://insta'
  },
  {
    'image':'linkedin.png',
    'link':'http://linkedin'
  },
  {
    'image':'m.png',
    'link':'http://m'
  },
  {
    'image':'mail.png',
    'link':'http://mail'
  },
  {
    'image':'social.png',
    'link':'http://social'
  },
  {
    'image':'tiktok.png',
    'link':'http://tiktok'
  },
   {
    'image':'rotate.png',
    'link':'http://rotate'
  },

];


class ConstantValue {
  static String deviceToken = '';
  static String appLanguage = '';
  static bool inApp = false;
  static String currentChatPeerID = '';
}