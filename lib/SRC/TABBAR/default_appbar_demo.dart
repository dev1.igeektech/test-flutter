

import 'package:demo/SRC/home.dart';
import 'package:demo/custom/src/bar.dart';
import 'package:demo/custom/src/item.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import 'components/choose_tab_item.dart';
import 'data.dart';
import 'model/badge.dart';
import 'model/choice_value.dart';

class DefaultAppBarDemo extends StatefulWidget {
  @override
  State createState() {
    return _State();
  }
}

class _State extends State<DefaultAppBarDemo>
    with SingleTickerProviderStateMixin {
  static const kStyles = [
    ChoiceValue<TabStyle>(
      title: 'TabStyle.reactCircle',
      label: 'Appbar use reactCircle style',
      value: TabStyle.reactCircle,
    ),

  ];

  static final kTabTypes = [
    ChoiceValue<List<TabItem>>(
      title: 'Icon Tab',
      label: 'Appbar use icon with Tab',
      value: Data.items(image: false),
    ),
    ChoiceValue<List<TabItem>>(
      title: 'Image Tab',
      label: 'Appbar use image with Tab',
      value: Data.items(image: true),
    ),
  ];
  var _tabItems = kTabTypes.first;

  ChoiceValue<TabStyle> _style = kStyles.first;
  ChoiceValue<Curve> _curve = Data.curves.first;
  Color _barColor = Data.namedColors.first.color;
  Gradient _gradient = Data.gradients.first;
  Badge _badge;
  TabController _tabController;
  TextDirection _textDirection = TextDirection.ltr;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: _tabItems.value.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    var options = <Widget>[
      // const Heading('Appbar Color'),
      // ColorsItem(Data.namedColors, _barColor, _onBarColorChanged),
      // const Heading('Background Gradient'),
      // GradientItem(Data.gradients, _gradient, _onGradientChanged),
      // const Heading('Badge Chip'),
      // ChipItem(Data.badges, _badge, _onBadgeChanged),
      // const Heading('Tab Type'),
      // ChooseTabItem(kTabTypes, _tabItems, _onTabItemTypeChanged),
      // const Heading('Tab Style'),
    ];
    // options.addAll(kStyles.map((s) => RadioItem<TabStyle>(s, _style,
    //     s.value == TabStyle.flip && kIsWeb ? null : _onStyleChanged)));
    if (_style.value != TabStyle.fixed &&
        _style.value != TabStyle.fixedCircle) {
      // options.add(const Heading('Animation Curve'));
      // options.addAll(
      //     Data.curves.map((c) => RadioItem<Curve>(c, _curve, _onCurveChanged)));
    }

    return Directionality(
      textDirection: _textDirection,
      child: Scaffold(
        body: TabBarView(
            controller: _tabController,
            physics:const NeverScrollableScrollPhysics(),
            
            children: [
              Home(),
              Home(),
              Home(),
              Home(),
              Home(),
            ]
            // _tabItems.value
            //     .map((i) => i.title == 'Home' || i.title == 'Happy'
            //         ? ListView(children: options)
            //         : Center(
            //             child: Text(
            //             '${i.title} World',
            //             style: TextStyle(fontSize: 30),
            //           )))
                .toList(growable: false)),
        bottomNavigationBar: _badge == null
            ? ConvexAppBar(
              height: 65,
              elevation: 0.9,
              curveSize: 68,
              activeColor: Colors.red,
               initialActiveIndex: 4,
                items: _tabItems.value,
                style: _style.value,
                curve: _curve.value,
                backgroundColor: Colors.white,
                color: Colors.grey,
                gradient: _gradient,
                controller: _tabController,
                onTap: (int i) => debugPrint('select index=$i'),
              )
            : ConvexAppBar.badge(
                {3: _badge.text, 4: Icons.assistant_photo, 2: Colors.redAccent},
                badgePadding: _badge.padding,
                badgeColor: _badge.badgeColor,
                badgeBorderRadius: _badge.borderRadius,
                items: _tabItems.value,
                style: _style.value,
                curve: _curve.value,
                backgroundColor: _barColor,
                gradient: _gradient,
                controller: _tabController,
                onTap: (int i) => debugPrint('select index=$i'),
              ),
      ),
    );
  }

  void _onTabItemTypeChanged(ChoiceValue<List<TabItem>> value) {
    setState(() {
      _tabItems = value;
    });
  }

  void _onStyleChanged(ChoiceValue<TabStyle> value) {
    setState(() {
      _style = value;
    });
  }

  void _onCurveChanged(ChoiceValue<Curve> value) {
    setState(() {
      _curve = value;
    });
  }

  void _onBarColorChanged(Color value) {
    setState(() {
      _barColor = value;
    });
  }

  void _onGradientChanged(Gradient value) {
    setState(() {
      _gradient = value;
    });
  }

  void _onBadgeChanged(Badge value) {
    setState(() {
      _badge = value;
    });
  }
}
