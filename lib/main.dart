import 'dart:async';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SRC/Language/LangModel.dart';
import 'SRC/app.dart';

void main() {
  
  runZoned(() async {
    WidgetsFlutterBinding.ensureInitialized();
    SharedPreferences.getInstance().then((prefs) {
      runApp(
              ScopedModel<LangModel>(
              model: LangModel(),
              child: MyApp(prefs: prefs),
            ),
          );
    });
  });
}
