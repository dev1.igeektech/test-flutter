/*
 *  Copyright 2020 Chaobin Wu <chaobinwu89@gmail.com>
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'package:demo/SRC/Language/Translations.dart';
import 'package:flutter/material.dart';

import '../item.dart';
import 'blend_image_icon.dart';
import 'inner_builder.dart';
import 'transition_container.dart';

/// Convex shape is moved after selection.
class ReactCircleTabStyle extends InnerBuilder {
  /// Color used as background of appbar and circle icon.
  final Color backgroundColor;

  /// Curve for tab transition.
  final Curve curve;

  /// Create style builder.
  ReactCircleTabStyle({
    List<TabItem> items,
    Color activeColor,
    Color color,
    this.backgroundColor,
    this.curve,
  }) : super(items: items, activeColor: activeColor, color: color);

  @override
  Widget build(BuildContext context, int index, bool active) {
    var item = items[index];
    var style = ofStyle(context);
    var margin = style.activeIconMargin;
    var textStyle = style.textStyle(color);

    if (active) {
      final item = items[index];
      return TransitionContainer.scale(
        
        child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Padding(
              padding:  EdgeInsets.only(bottom: 15),
              child: Container(
           margin: EdgeInsets.all(12),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: active ? activeColor : color,
          ),
          child: BlendImageIcon(
              active ? item.activeIcon ?? item.icon : item.icon,
              size: style.activeIconSize -8,
              color: item.blend ? backgroundColor : null,
          ),
        ),
            ),


        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 6.5),
            child: Text(index == 0 ? 'Home'     :
                        index == 1 ? 'Location' :  
                        index == 2 ? 'Activity' :  
                        index == 3 ? 'Chat'     : 'My Profile'
                        
                        //  Translations.of(context).myProfile
            ,style: TextStyle(color: Colors.red,fontSize: 11.5),),
          ))
          ],
        ),
        curve: curve,
      );
    }
   
    return Container(
      padding: EdgeInsets.only(bottom: 2),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          BlendImageIcon(
            active ? item.activeIcon ?? item.icon : item.icon,
            color: item.blend ? color : null,
          ),
          style.hideEmptyLabel && (item.title == null || item.title.isEmpty)
              ? null
              : Text(item.title ?? '', style: textStyle)
        ]..removeWhere((it) => it == null),
      ),
    );
  }
}
      //                         "myProfile":'My Profile',
      // 'chat':'Chat',
      // 'activity':'Activity',
      // 'location':'Location',
      // 'home':'Home',